#!/usr/bin/env bash
set -euo pipefail
IFACE=${IFACE:-}

echo "IFACE: ${IFACE}"
