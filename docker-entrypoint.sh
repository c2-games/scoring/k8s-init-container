#!/usr/bin/env bash
set -euo pipefail
DATE="date -Iseconds"

echo "$(eval ${DATE}) INFO --- Execution started ---"

# Generate Kube-OVN TLS pre-reqs
function generateOVSPKI() {
    # Check if kube-ovn-tls secret exists
    kube_ovn_tls_exists=$(kubectl get secret -n kube-system kube-ovn-tls --ignore-not-found)

    if [[ -z $kube_ovn_tls_exists ]];then
        echo "$(eval ${DATE}) INFO Generating OVS PKI pre-requisites"
        dir=$(mktemp -d)

        # Initialize OVS PKI
        echo "$(eval ${DATE}) INFO Initializing OVS PKI"
        ovs-pki init -l /dev/stdout --force

        # Copy OVS PKI CA Cert to temp dir
        echo "$(eval ${DATE}) INFO Copying CA certificate to temp directory"
        cp /var/lib/openvswitch/pki/switchca/cacert.pem ${dir}

        # cd to temp dir
        cd ${dir}

        # Generate OVS PKI CSR
        echo "$(eval ${DATE}) INFO Generating OVS PKI CSR"
        ovs-pki req ovn -l /dev/stdout --force

        # Sign and mint OVS PKI cert
        echo "$(eval ${DATE}) INFO Signing OVS PKI CSR and minting certificate"
        ovs-pki -b sign ovn -l /dev/stdout --force

        # Create Kube-OVN TLS Secret from OVS PKI assets
        echo "$(eval ${DATE}) INFO Creating kube-ovn-tls secret from OVS PKI assets"
        kubectl create secret generic -n kube-system kube-ovn-tls --from-file=cacert=cacert.pem --from-file=cert=ovn-cert.pem --from-file=key=ovn-privkey.pem

        cd /

        # Clean up PKI files
        echo "$(eval ${DATE}) INFO Cleaning up temporary directory"
        rm -rf ${dir}
        unset dir
    else
        echo "$(eval ${DATE}) INFO kube-ovn-tls secret already exists. Skipping OVS PKI generation"
    fi
}


# Perform Kube-OVN install
function installKubeOVN() {
    echo "$(eval ${DATE}) INFO Beginning installation of Kube-OVN"

    # change directory to /opt/kube-ovn/ so that install script can
    # write files to directory
    cd /opt/kube-ovn/

    export REGISTRY="kubeovn"                            # Image Repo
    export VERSION="v1.13.1"                             # Image Tag
    export POD_CIDR="10.16.0.0/16"                       # Default subnet CIDR don't overlay with SVC/NODE/JOIN CIDR
    export SVC_CIDR="10.96.0.0/12"                       # Be consistent with apiserver's service-cluster-ip-range
    export JOIN_CIDR="100.64.0.0/16"                     # Pod/Host communication Subnet CIDR, don't overlay with SVC/NODE/POD CIDR
    export LABEL="node-role.kubernetes.io/control-plane" # The node label to deploy OVN DB
    export IFACE=""                                      # The name of the host NIC used by the container network, or if empty use the NIC that host Node IP in Kubernetes
    export TUNNEL_TYPE="geneve"                          # Tunnel protocol，available options: geneve, vxlan or stt. stt requires compilation of ovs kernel module
    export IFACE="ens*"                                  # Use all interfaces with names matching ens* regex
    export ENABLE_IC="true"                              # Enable Interconnect
    bash install.sh
}

function installLinkerd() {
    # Download linkerd client and install
    echo "$(eval ${DATE}) INFO Downloading and installing linkerd client"
    curl --proto '=https' --tlsv1.2 -sSfL https://run.linkerd.io/install | sh

    # Add linkerd client to PATH
    export PATH=$PATH:$HOME/.linkerd2/bin

    # Check linkerd installation
    echo "$(eval ${DATE}) INFO Running linkerd pre-requisite sanity checks"
    linkerd check --pre

    # Create linkerd and linkerd-viz namespaces
    echo "$(eval ${DATE}) INFO Creating the linkerd namespace"
    kubectl create namespace linkerd
    echo "$(eval ${DATE}) INFO Creating the linkerd-viz namespace"
    kubectl create namespace linkerd-viz

    # Annotate namespaces with default tolerations
    echo "$(eval ${DATE}) INFO Annotating the linkerd namespace with default tolerations"
    kubectl annotate namespace linkerd 'scheduler.alpha.kubernetes.io/defaultTolerations'='[{"operator": "Exists", "effect": "NoSchedule", "key": "node-role.kubernetes.io/control-plane"}]'
    echo "$(eval ${DATE}) INFO Annotating the linkerd-viz namespace with default tolerations"
    kubectl annotate namespace linkerd-viz 'scheduler.alpha.kubernetes.io/defaultTolerations'='[{"operator": "Exists", "effect": "NoSchedule", "key": "node-role.kubernetes.io/control-plane"}]'

    # Install linkerd Custom Resource Defintions
    echo "$(eval ${DATE}) INFO Installing linkerd Custom Resource Definitions"
    linkerd install --crds | kubectl apply -f -

    # Get ClusterDomain Name from coredns Corefile config
    echo "$(eval ${DATE}) INFO Gathering ClusterDomain name from coreDNS"
    cluster_domain=$(kubectl  get cm coredns -n kube-system -o jsonpath='{.data.Corefile}' | grep kubernetes | awk '{print $2}')

    # Install linkerd control-plane to linkerd namespace
    echo "$(eval ${DATE}) INFO Installing linkerd control-plane"
    linkerd install --cluster-domain=${cluster_domain} --identity-trust-domain=${cluster_domain} | kubectl apply -f -

    # Install linkderd viz-extension
    echo "$(eval ${DATE}) INFO Installing linkerd viz-extension"
    linkerd viz install --set clusterDomain=${cluster_domain} | kubectl apply -f -

    # Ensure linkerd was installed correctly
    echo "$(eval ${DATE}) INFO Validating linkerd installation"
    linkerd check

    # Annotate namespaces other than kube-system
    # This should probably be done within either namespace or workload manifests
    #kubectl annotate namespaces ${namespace} 'linkerd.io/inject=enabled
}


function installMultus() {
    # Install Multus via multus-daemonset manifest
    echo "$(eval ${DATE}) INFO Installing Multus CNI meta-plugin daemonset"
    kubectl apply -f /opt/manifests/multus-daemonset.yml
}

function cloneKubernetesResourcesRepo() {

    # Clone Kube-OVN baseline manifests
    git clone https://gitlab.com/c2-games/infrastructure/kubernetes.git /opt/kubernetes
}

function deployKubeOVNBaselines() {
    echo "$(eval ${DATE}) INFO Deploying baseline Kube-OVN resources"

    # Deploy baseline Kube-OVN resources
    kubectl apply -f /opt/kubernetes/kube-ovn/
}

function deployNamespaces() {
    echo "$(eval ${DATE}) INFO Deploying k8s namespaces"

    # Deploy baseline Kube-OVN resources
    kubectl apply -f /opt/kubernetes/namespaces/
}

function deployRBAC() {
    echo "$(eval ${DATE}) INFO Deploying k8s RBAC resources"

    # Deploy baseline Kube-OVN resources
    kubectl apply -f /opt/kubernetes/user-accounts/rbac/
}

generateOVSPKI
installKubeOVN
installLinkerd
installMultus
cloneKubernetesResourcesRepo
deployKubeOVNBaselines
deployNamespaces
deployRBAC

echo "$(eval ${DATE}) INFO --- Execution completed successfully ---"
exit 0
