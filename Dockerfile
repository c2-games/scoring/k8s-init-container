FROM alpine:3.17.1

ARG kube_ver="v1.31.2"
ARG UNAME=abc
ARG UID=1000

RUN apk -U upgrade --no-cache \
    && apk add --no-cache --update \
        bash \
        curl \
        openssl \
        openvswitch \
        git \
    && curl -LO "https://dl.k8s.io/release/${kube_ver}/bin/linux/amd64/kubectl" \
    && curl -LO "https://dl.k8s.io/release/${kube_ver}/bin/linux/amd64/kubectl.sha256" \
    && echo "$(cat kubectl.sha256)  kubectl" | sha256sum -c \
    && install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl \
    && rm -f /kubectl /kubectl.sha256 \
    && adduser -D -H -h /opt -s /bin/false -u $UID $UNAME \
    && rm -f /kubectl /kubectl.sha256 \
    && touch /usr/local/bin/kubectl-ko \
    && chown $UID. /usr/local/bin/kubectl-ko

COPY ./docker-entrypoint.sh /
COPY ./kube-ovn/ /opt/kube-ovn/
COPY ./manifests/ /opt/manifests/

RUN chmod +x /docker-entrypoint.sh \
    && chown -R $UID. /var/lib/openvswitch/ /opt/


USER $UNAME

ENTRYPOINT /docker-entrypoint.sh
